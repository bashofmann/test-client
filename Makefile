IMAGE=bashofmann/test-client
TAG=dev

build:
	docker build -t $(IMAGE):$(TAG) .

push: build
	docker push $(IMAGE):$(TAG)

run: build
	docker run --name test-client --net=host --rm $(IMAGE):$(TAG)

deploy: push
	helm upgrade --install test-client ./chart/test-client --namespace test --create-namespace --set image.tag=$(TAG)
