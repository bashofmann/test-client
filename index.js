const request = require('request');

const target_hostname = process.env.TARGET_HOSTNAME || 'localhost';
const target_port = process.env.TARGET_PORT || '8000';

const interval = setInterval(() => {
  request(`http://${target_hostname}:${target_port}`, (error, response, body) =>{
    console.error('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
  });
}, 1000);

const close = () => {
  console.info('SIGTERM signal received.');
  clearInterval(interval);
  process.exit(0);
}

process.on('SIGTERM', close);
process.on('SIGINT', close);
